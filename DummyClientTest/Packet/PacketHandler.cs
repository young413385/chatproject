﻿using DummyClientTest;
using Google.Protobuf;
using Google.Protobuf.Protocol;
using ServerCore;
using System;
using System.Collections.Generic;
using System.Text;

class PacketHandler
{
	public static void S_EnterRoomHandler(PacketSession session, IMessage packet)
	{
		S_EnterRoom enterRoomPacket = packet as S_EnterRoom;
		ServerSession serverSession = session as ServerSession;

		serverSession.AIUser.OnEnterRoom(enterRoomPacket);
	}

	public static void S_LeaveRoomHandler(PacketSession session, IMessage packet)
	{
		S_LeaveRoom leaveRoomPacket = packet as S_LeaveRoom;
		ServerSession serverSession = session as ServerSession;

		serverSession.AIUser.OnLeaveRoom(leaveRoomPacket);
	}

	public static void S_ChatMsgHandler(PacketSession session, IMessage packet)
	{
		S_ChatMsg chatMsgPacket = packet as S_ChatMsg;
		ServerSession serverSession = session as ServerSession;

		serverSession.AIUser.OnChatMsg(chatMsgPacket);
	}

	public static void S_LoginHandler(PacketSession session, IMessage packet)
	{
		S_Login loginPacket = packet as S_Login;
		ServerSession serverSession = session as ServerSession;

		serverSession.AIUser.OnLogin(loginPacket);
	}

	public static void S_RoomsInfoHandler(PacketSession session, IMessage packet)
	{
		S_RoomsInfo roomsInfoPacket = packet as S_RoomsInfo;
		ServerSession serverSession = session as ServerSession;

		serverSession.AIUser.OnRoomsInfo(roomsInfoPacket);
	}


}
