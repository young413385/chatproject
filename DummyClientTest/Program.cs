﻿using ServerCore;
using System;
using Google.Protobuf.Protocol;
using System.Net;
using System.Threading;

namespace DummyClientTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
			AIUserViewer.Instance.StartUserView();

			string host = Dns.GetHostName();
			IPHostEntry ipHost = Dns.GetHostEntry(host);
			IPAddress ipAddr = ipHost.AddressList[0];
			IPEndPoint endPoint = new IPEndPoint(ipAddr, 7777);

			Connector connector = new Connector();

			connector.Connect(endPoint,
				() => { return SessionManager.Instance.Generate(); },
				10);

            while (true)
            {

            }
		}
    }
}
