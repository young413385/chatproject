﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DummyClientTest
{
    public enum UserState {
        BeforeLogin,
        InLobby,
        InRoom
    }

    public enum BeforeLoginBehavior
    {
        Wait,
        Login
    }

    public enum InLobbyBehavior
    {
        Wait,
        JoinRoom,
        MakeRoom
    }

    public enum InRoomBehavior
    {
        Wait,
        SendMsg,
        LeaveRoom
    }

    public class AIUser
    {
        static int UserNameCount = -1;
        static string[] UserNames =
        { 
            "Rufus", "Bear", "Dakota", "Fido","Vanya", 
            "Samuel", "Koani", "Volodya","Prince", "Yiska",
            "Maggie", "Penny", "Saya", "Princess","Abby", 
            "Laila", "Sadie", "Olivia","Starlight", "Talla"
        };

        static string[] ChatMessage =
        {
            "Hello", "Hi", "Nice to see you!", "Who are you?", "How was your day?", "How are you doing?",
            "What is up?", "How is it going?", "What is new?", "What is going on?", "How is everything?",
            "Good to see you!", "Long time no see", "It is been a while", "How have you been?"
        };
        int index;

        public ServerSession Session { get;}

        public UserState State = UserState.BeforeLogin;

        public string _username = string.Empty;
        public RoomInfo _roomInfo = null;

        public Dictionary<int, RoomInfo> _rooms = new Dictionary<int, RoomInfo>();
        public List<ChatRecord> _chatRecords = null;

        object _lock = new object();

        Random _random = new Random();
        public AIUser(ServerSession session)
        {
            Session = session;
            StartAI();
            index = Interlocked.Increment(ref UserNameCount);
        }

        public void StartAI()
        {
            System.Timers.Timer _timer = new System.Timers.Timer();
            _timer.Interval = _random.Next(1500, 3000);
            _timer.Elapsed += ((s, e) => { DoRandomBehavior(); });
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        public void DoRandomBehavior()
        {
            int ranNum = _random.Next(10);
            switch (State)
            {
                case UserState.BeforeLogin:
                    if(ranNum > 4)
                    {
                        SendLogin(UserNames[index]);
                    }
                    else
                    {
                        //Wait...
                    }
                    break;
                case UserState.InLobby:
                    if (ranNum > 8)
                    {
                        SendEnterRoom(0, RoomEnterType.New);
                    }
                    else if(ranNum > 3)
                    {
                        lock (_lock)
                        {
                            if (_rooms.Count == 0)
                                SendEnterRoom(0, RoomEnterType.New);
                            else
                            {
                                SendEnterRoom(_rooms.Values.ToArray()[_random.Next(_rooms.Count)].RoomId, RoomEnterType.Join);
                            }
                        }
                    }
                    else
                    {
                        //Wait...
                    }
                    break;
                case UserState.InRoom:
                    if (ranNum > 7)
                    {
                        SendLeaveRoom();
                    }
                    else if (ranNum > 2)
                    {
                        SendChatMsg(ChatMessage[_random.Next(ChatMessage.Length)]);
                    }
                    else
                    {
                        //Wait...
                    }
                    break;
            }
        }

        public void OnEnterRoom(S_EnterRoom enterRoomPacket)
        {
            lock (_lock)
            {
                _roomInfo = enterRoomPacket.RoomInfo;
                _chatRecords = enterRoomPacket.ChatRecords.ToList();
                _rooms.Clear();
                State = UserState.InRoom;
            }
        }

        public void OnLeaveRoom(S_LeaveRoom leaveRoomPacket)
        {
            lock (_lock)
            {
                _rooms.Clear();
                foreach (RoomInfo roomInfo in leaveRoomPacket.CurrentRoomsInfo)
                {
                    _rooms.Add(roomInfo.RoomId, roomInfo);
                }
                _roomInfo = null;
                _chatRecords = null;

                State = UserState.InLobby;
            }
        }
        public void OnChatMsg(S_ChatMsg chatMsgPacket)
        {
            lock (_lock)
            {
                if(_chatRecords!= null)
                    _chatRecords.Add(chatMsgPacket.ChatRecord);
            }
        }
        public void OnLogin(S_Login loginPacket)
        {
            _rooms.Clear();
            foreach (RoomInfo roomInfo in loginPacket.CurrentRoomsInfo)
            {
                _rooms.Add(roomInfo.RoomId, roomInfo);
            }
            _username = loginPacket.UserName;
            State = UserState.InLobby;
        }
        public void OnRoomsInfo(S_RoomsInfo roomsInfoPacket)
        {
            switch (roomsInfoPacket.RoomsInfoType)
            {
                case RoomsInfoType.InLobby:
                    foreach (int roomId in roomsInfoPacket.RemoveRoomIdList)
                    {
                        _rooms.Remove(roomId);
                    }

                    foreach (RoomInfo roomInfo in roomsInfoPacket.AddRoomInfoList)
                    {
                        //_rooms[roomInfo.RoomId] = roomInfo;
                        _rooms.Add(roomInfo.RoomId, roomInfo);
                    }
                    break;
            }
        }

        public void SendEnterRoom(int roomId, RoomEnterType roomEnterType)
        {
            C_EnterRoom enterRoomPacket = new C_EnterRoom();
            enterRoomPacket.RoomId = roomId;
            enterRoomPacket.RoomEnterType = roomEnterType;
            Session.Send(enterRoomPacket);
        }

        public void SendLeaveRoom()
        {
            C_LeaveRoom leaveRoomPacket = new C_LeaveRoom();
            Session.Send(leaveRoomPacket);
        }

        public void SendChatMsg(string message)
        {
            C_ChatMsg chatMsg = new C_ChatMsg();
            chatMsg.Message = message;
            Session.Send(chatMsg);
        }

        public void SendLogin(string userName)
        {
            C_Login login = new C_Login();
            login.UserName = userName;
            Session.Send(login);
        }

    }
}
