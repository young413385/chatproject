﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DummyClientTest
{
    public class AIUserViewer
    {
        public static AIUserViewer Instance { get; } = new AIUserViewer();

        object _lock = new object();
        List<AIUser> _users = new List<AIUser>();
        string _beforeLoginSessionIds = string.Empty;
        List<string[]> _lobbyTemp = new List<string[]>();
        List<string[]> _lobby = new List<string[]>();

        Dictionary<int, List<string[]>> _roomTemp = new Dictionary<int, List<string[]>>();
        Dictionary<int, List<string[]>> _room = new Dictionary<int, List<string[]>>();

        public void AddUser(AIUser aiUser)
        {
            lock (_lock)
            {
                _users.Add(aiUser);
            }
        }

        public void StartUserView()
        {
            Console.SetWindowSize(150, 80);
            System.Timers.Timer _timer = new System.Timers.Timer();
            _timer.Interval = 200;
            _timer.Elapsed += ((s, e) => { UpdateView(); });
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        void UpdateView()
        {
            //return;
            UpdateData();
            ClearVisibleRegion();
            Console.WriteLine("-----Before Login Sessions----------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"    {_beforeLoginSessionIds}");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("-----Users In Lobby-----------------------------------------------------------------------------------------------------------------------------------");
            lock (_lock)
            {
                for (int i = 0; i < _lobby.Count; i++)
                {
                    for (int j = 0; j < 12; j++)
                    {
                        Console.WriteLine(_lobby[i][j]);
                    }
                }

                Console.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("-----Users In Chat Room-------------------------------------------------------------------------------------------------------------------------------");

                foreach (List<string[]> room in _room.Values)
                {
                    for (int i = 0; i < room.Count; i++)
                    {
                        for (int j = 0; j < 12; j++)
                        {
                            Console.WriteLine(room[i][j]);
                        }
                    }
                }
            }
            

        }


        void UpdateData()
        {
            _beforeLoginSessionIds = "";
            _lobbyTemp.Clear();
            _lobby.Clear();
            _room.Clear();
            _roomTemp.Clear();
            lock (_lock)
            {
                foreach (AIUser aiUser in _users)
                {
                    switch (aiUser.State)
                    {
                        case UserState.BeforeLogin:
                            _beforeLoginSessionIds += aiUser.Session.SessionId + ", ";
                            break;
                        case UserState.InLobby:
                            _lobbyTemp.Add(GetLobbyView(aiUser));
                            if (_lobbyTemp.Count == 5)
                            {
                                string[] lobbyAddTemp = new string[12];
                                foreach (string[] lobby in _lobbyTemp)
                                {
                                    for (int i = 0; i < 12; i++)
                                    {
                                        lobbyAddTemp[i] += lobby[i];
                                    }
                                }
                                _lobby.Add(lobbyAddTemp);
                                _lobbyTemp.Clear();
                            }
                            break;
                        case UserState.InRoom:
                            if (!_roomTemp.ContainsKey(aiUser._roomInfo.RoomId))
                                _roomTemp.Add(aiUser._roomInfo.RoomId,new List<string[]>());
                            _roomTemp[aiUser._roomInfo.RoomId].Add(GetChatRoomView(aiUser));
                            if (_roomTemp[aiUser._roomInfo.RoomId].Count == 5)
                            {
                                string[] roomAddTemp = new string[12];
                                foreach (string[] room in _roomTemp[aiUser._roomInfo.RoomId])
                                {
                                    for (int i = 0; i < 12; i++)
                                    {
                                        roomAddTemp[i] += room[i];
                                    }
                                }
                                if (!_room.ContainsKey(aiUser._roomInfo.RoomId))
                                    _room.Add(aiUser._roomInfo.RoomId, new List<string[]>());
                                _room[aiUser._roomInfo.RoomId].Add(roomAddTemp);
                                _roomTemp[aiUser._roomInfo.RoomId].Clear();
                            }
                            break;
                    }
                }
            }

            //
            if(_lobbyTemp.Count > 0)
            {
                string[] lobbyAddTemp = new string[12];
                foreach (string[] lobby in _lobbyTemp)
                {
                    for (int i = 0; i < 12; i++)
                    {
                        lobbyAddTemp[i] += lobby[i];
                    }
                }
                _lobby.Add(lobbyAddTemp);
                _lobbyTemp.Clear();
            }

            foreach (KeyValuePair<int, List<string[]>> roomTemp in _roomTemp.ToArray())
            {
                if (roomTemp.Value.Count > 0)
                {
                    string[] roomAddTemp = new string[12];
                    foreach (string[] room in roomTemp.Value)
                    {
                        for (int i = 0; i < 12; i++)
                        {
                            roomAddTemp[i] += room[i];
                        }
                    }
                    if (!_room.ContainsKey(roomTemp.Key))
                        _room[roomTemp.Key] = new List<string[]>();
                    _room[roomTemp.Key].Add(roomAddTemp);
                    _roomTemp[roomTemp.Key].Clear();
                }
            }
            
        }

        string[] GetChatRoomView(AIUser aiUser)
        {
            string temp = "                              ";
            string[] ret = new string[12]
            {
                "------------------------------",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "------------------------------"
            };

            ret[1] = "- UserName : " + aiUser._username;
            ret[1] += temp.Substring(0, 29 - ret[1].Length) + "-";

            ret[2] = "- < " + aiUser._roomInfo.RoomName + " >";
            ret[2] += temp.Substring(0, 29 - ret[2].Length) + "-";

            List<ChatRecord> chatRecords = aiUser._chatRecords;

            int from = Math.Max(chatRecords.Count - 8, 0);
            for (int i = from; i < chatRecords.Count; i++)
            {
                ret[i + 3 - from] = $"- {chatRecords[i].UserInfo.UserName}-{chatRecords[i].Message}";
                if (ret[i + 3 - from].Length > 29)
                {
                    ret[i + 3 - from] = ret[i + 3 - from].Substring(0, 29) + "-";
                }
                else
                {
                    ret[i + 3 - from] += temp.Substring(0, 29 - ret[i + 3 - from].Length) + "-";
                }
            }

            return ret;
        }

        string[] GetLobbyView(AIUser aiUser)
        {
            string temp = "                              ";
            string[] ret = new string[12] 
            {
                "------------------------------",
                "-                            -",
                "-      <Chat Room List>      -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "-                            -",
                "------------------------------"
            };

            ret[1] = "- UserName : " + aiUser._username;
            ret[1] += temp.Substring(0, 29 - ret[1].Length) + "-";

            List<RoomInfo> roomInfos = aiUser._rooms.Values.ToList();
            int from = Math.Max(roomInfos.Count - 8, 0);
            for(int i = from ; i < roomInfos.Count; i++)
            {
                ret[i + 3 - from] = $"- {roomInfos[i].RoomName}";
                if(ret[i + 3 - from].Length > 29)
                {
                    ret[i + 3 - from] = ret[i + 3 - from].Substring(0, 29) + "-";
                }
                else
                {
                    ret[i + 3 - from] += temp.Substring(0, 29 - ret[i + 3 - from].Length) + "-";
                }
            }

            return ret;
        }

        void ClearVisibleRegion()
        {
            for (int y = Console.WindowTop; y < Console.WindowTop + Console.WindowHeight; y++)
            {
                Console.SetCursorPosition(Console.WindowLeft, y);
                Console.Write(new string(' ', Console.WindowWidth));
            }

            Console.SetCursorPosition(Console.WindowLeft, Console.WindowTop);
        }

    }
}
