﻿using Google.Protobuf;
using Google.Protobuf.Protocol;
using System;
using System.Timers;
using System.Collections.Generic;
using System.Text;
using ServerCore;

namespace Server
{
	public class ChatRoom : JobSerializer
	{
		public RoomInfo ChatRoomInfo { get; set; }

		Dictionary<int, User> _users = new Dictionary<int, User>();
		List<ChatRecord> _chatRecords = new List<ChatRecord>();

		Timer _timer;

		public void Init(RoomInfo roomInfo, int tick = 100)
		{
			ChatRoomInfo = roomInfo;

			_timer = new Timer();
			_timer.Interval = tick;
			_timer.Elapsed += ((s, e) => { Update(); });
			_timer.AutoReset = true;
			_timer.Enabled = true;
		}

		public void Update()
		{
			Flush();
		}

		public void EnterChatRoom(User user)
		{
			if (_users.ContainsKey(user.UserInfo.UserId))
				return;
			user.CurrentChatRoom = this;
			_users.Add(user.UserInfo.UserId, user);

			S_EnterRoom enterRoom = new S_EnterRoom();
			enterRoom.RoomInfo = ChatRoomInfo;
			enterRoom.ChatRecords.AddRange(_chatRecords);

			user._userState = (int)UserState.InRoom;
			user.Session.Send(enterRoom);

			Push(ChatMsg, user, $"{user.UserInfo.UserName} Join");
		}

		public void LeaveChatRoom(User user)
		{
			if (!_users.Remove(user.UserInfo.UserId))
				return;

			if (_users.Values.Count > 0)
            {
				Push(ChatMsg, user, $"{user.UserInfo.UserName} Leave");
			}
            else
            {
				_timer.Stop();
				RoomManager.Instance.Push(RoomManager.Instance.RemoveChatRoom,ChatRoomInfo.RoomId);
            }
		}

		public void ChatMsg(User user, string message)
        {

			if(message.Equals(string.Empty) || user == null)
            {
				return;
            }
			ChatRecord chatRecord = new ChatRecord();
			chatRecord.UserInfo = user.UserInfo;
			chatRecord.Message = message;
			_chatRecords.Add(chatRecord);

			S_ChatMsg chatMsg = new S_ChatMsg();
			chatMsg.ChatRecord = chatRecord;

			Broadcast(chatMsg);
        }

		public void Broadcast(IMessage packet)
		{
			foreach (User user in _users.Values)
			{
				user.Session.Send(packet);
			}
		}
	}
}
