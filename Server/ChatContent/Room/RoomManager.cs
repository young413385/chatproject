﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Timers;
using Google.Protobuf.Protocol;
using ServerCore;

namespace Server
{
	public class RoomManager : JobSerializer
	{
		public static RoomManager Instance { get; } = new RoomManager();

		System.Timers.Timer _roomManagerTimer;

		Dictionary<int, ChatRoom> _chatRooms = new Dictionary<int, ChatRoom>();
		Dictionary<int, RoomInfo> _roomsInfo = new Dictionary<int, RoomInfo>();

		Queue<RoomInfo> _addRoomList = new Queue<RoomInfo>();
		Queue<int> _removeRoomList = new Queue<int>();

		int _roomId = 0;

		public void Init(int tick = 100)
        {
			var timer = new System.Timers.Timer();
			timer.Interval = tick;
			timer.Elapsed += ((s, e) => { Update(); });
			timer.AutoReset = true;
			timer.Enabled = true;

			_roomManagerTimer = timer;
		}

		void Update()
        {
			Flush();
			Push(UpdateLobbyRooms);
        }

		public void UpdateLobbyRooms()
        {
			S_RoomsInfo roomsInfo = new S_RoomsInfo();
			roomsInfo.RoomsInfoType = RoomsInfoType.InLobby;
			if (_addRoomList.Count == 0 && _removeRoomList.Count == 0)
				return;

			while (_removeRoomList.Count > 0)
			{
				int roomId = _removeRoomList.Dequeue();
				roomsInfo.RemoveRoomIdList.Add(roomId);
				_roomsInfo.Remove(roomId);
			}
			while (_addRoomList.Count > 0)
            {
                RoomInfo roomInfo = _addRoomList.Dequeue();
				roomsInfo.AddRoomInfoList.Add(roomInfo);
				_roomsInfo.Add(roomInfo.RoomId, roomInfo);
			}

			UserManager.Instance.Push(UserManager.Instance.UpdateLobbyRooms,roomsInfo);
		}

		public void GenerateChatRoom(string roomName, ClientSession clientSession)
		{
			ChatRoom chatRoom = new ChatRoom();
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.RoomName = roomName;
			roomInfo.RoomId = _roomId;
			chatRoom.Init(roomInfo);
			_chatRooms.Add(_roomId, chatRoom);
			_addRoomList.Enqueue(roomInfo);

			_roomId++;

			chatRoom.Push(chatRoom.EnterChatRoom, clientSession.ThisUser);
		}

		public void RemoveChatRoom(int roomId)
		{
			_chatRooms.Remove(roomId);
			_removeRoomList.Enqueue(roomId);
		}

		public ChatRoom Find(int roomId)
		{
			ChatRoom room = null;
			if (_chatRooms.TryGetValue(roomId, out room))
				return room;

			return null;
		}

		public void OnLogin(ClientSession clientSession)
		{
			User user = clientSession.ThisUser;
			if (user == null)
				return;
			UserState prevUserState = (UserState)Interlocked.Exchange(ref user._userState, (int)UserState.TryLogin);
			if (prevUserState != UserState.BeforeLogin)
				return;

			S_Login s_Login = new S_Login();
			s_Login.UserName = clientSession.ThisUser.UserInfo.UserName;

			foreach (ChatRoom room in _chatRooms.Values)
			{
				s_Login.CurrentRoomsInfo.Add(room.ChatRoomInfo);
			}

			clientSession.ThisUser._userState = (int)UserState.InLobby;
			clientSession.Send(s_Login);
		}

		public void EnterRoom(ClientSession clientSession, C_EnterRoom enterRoomPacket)
		{
			User user = clientSession.ThisUser;
			if (user == null)
				return;
			UserState prevUserState = (UserState)Interlocked.Exchange(ref user._userState, (int)UserState.TryEnterRoom);
			if (prevUserState != UserState.InLobby)
				return;

			ChatRoom chatRoom = null;

			if (clientSession.ThisUser.CurrentChatRoom != null)
				return;

			switch (enterRoomPacket.RoomEnterType)
            {
				case RoomEnterType.Join:
					if(_chatRooms.TryGetValue(enterRoomPacket.RoomId, out chatRoom))
                    {
						chatRoom.Push(chatRoom.EnterChatRoom, clientSession.ThisUser);
                    }
                    else
                    {
						return;
                    }
					break;
				case RoomEnterType.New:
					Push(GenerateChatRoom,$"{clientSession.ThisUser.UserInfo.UserName} Chat Room", clientSession);
					break;
            }

        }

		public void LeaveRoom(ClientSession clientSession)
		{
			User user = clientSession.ThisUser;
			if (user == null)
				return;
			UserState prevUserState = (UserState)Interlocked.Exchange(ref user._userState, (int)UserState.TryLeaveRoom);
			if (prevUserState == UserState.InRoom)
				return;

			ChatRoom chatRoom = user.CurrentChatRoom;

			if (chatRoom == null)
				return;

			user.CurrentChatRoom = null;

			S_LeaveRoom leaveRoom = new S_LeaveRoom();

			foreach (ChatRoom room in _chatRooms.Values)
            {
				leaveRoom.CurrentRoomsInfo.Add(room.ChatRoomInfo);
			}
			user._userState = (int)UserState.InLobby;
			user.Session.Send(leaveRoom);

			chatRoom.Push(chatRoom.LeaveChatRoom, clientSession.ThisUser);

		}

	}
}
