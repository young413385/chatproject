﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Text;

namespace Server
{
	public enum UserState
    {
		BeforeLogin,
		TryLogin,
		InLobby,
		TryEnterRoom,
		InRoom,
		TryLeaveRoom
    }

	public class User
	{
		public ClientSession Session { get; set; }
		public UserInfo UserInfo { get; } = new UserInfo();

		public int _userState = (int)UserState.BeforeLogin;

		public UserState UserState { get { return (UserState)_userState; } }

		public ChatRoom CurrentChatRoom { get; set; } = null;
		public void SetUserName(string userName) { UserInfo.UserName = userName; }
    }
}
