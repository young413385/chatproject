﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerCore;
using System.Timers;

namespace Server
{
	public class UserManager : JobSerializer
	{
		public static UserManager Instance { get; } = new UserManager();

		object _lock = new object();
		Dictionary<int, User> _users = new Dictionary<int, User>();

		int _userCounter = 0;

		public void Init(int tick = 100)
		{
			var timer = new Timer();
			timer.Interval = tick;
			timer.Elapsed += ((s, e) => { Update(); });
			timer.AutoReset = true;
			timer.Enabled = true;
		}

		void Update()
        {
			Flush();
		}

		public User GenerateUser() 
		{
			User user = new User();
			lock (_lock)
			{
				user.UserInfo.UserId = _userCounter++;
				_users.Add(user.UserInfo.UserId, user);
			}

			return user;
		}

		public void UpdateLobbyRooms(S_RoomsInfo roomsInfo)
        {
			List<User> users = _users.Values.ToList();
			foreach (User user in users)
			{
				if(user.UserState == UserState.InLobby)
					user.Session.Send(roomsInfo);
			}
		}
	}
}
