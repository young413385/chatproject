﻿using Google.Protobuf;
using Google.Protobuf.Protocol;
using Server;
using ServerCore;

class PacketHandler
{
	public static void C_EnterRoomHandler(PacketSession session, IMessage packet)
	{
		C_EnterRoom enterPacket = packet as C_EnterRoom;
		ClientSession clientSession = session as ClientSession;

		RoomManager.Instance.Push(RoomManager.Instance.EnterRoom,clientSession, enterPacket);
	}

	public static void C_LeaveRoomHandler(PacketSession session, IMessage packet)
	{
		C_LeaveRoom leavePacket = packet as C_LeaveRoom;
		ClientSession clientSession = session as ClientSession;

		User user = clientSession.ThisUser;
		if (user == null)
			return;

		RoomManager.Instance.Push(RoomManager.Instance.LeaveRoom,clientSession);
	}

	public static void C_ChatMsgHandler(PacketSession session, IMessage packet)
	{
		C_ChatMsg chatMsgPacket = packet as C_ChatMsg;
		ClientSession clientSession = session as ClientSession;

		User user = clientSession.ThisUser;

		if (user == null)
			return;

		ChatRoom chatRoom = user.CurrentChatRoom;

		if (chatRoom == null)
			return;

		chatRoom.Push(chatRoom.ChatMsg, user, chatMsgPacket.Message);
	}

	public static void C_LoginHandler(PacketSession session, IMessage packet)
	{
		C_Login loginPacket = packet as C_Login;
		ClientSession clientSession = session as ClientSession;

		User user = clientSession.ThisUser;
		if (user == null)
			return;

		user.SetUserName(loginPacket.UserName);

		RoomManager.Instance.Push(RoomManager.Instance.OnLogin,clientSession);
	}

}
